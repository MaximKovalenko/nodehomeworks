const express = require('express');
const createFile = require('./createFile')
const getFiles = require('./checkFiles')
const getFile = require('./getFiles')
const app = express();

app.use(express.json());

app.use('/api/files', createFile);
app.use('/api/files', getFiles);
app.use('/', getFile);

app.listen(8080, () => {
    console.log('Server works at localhost:8080');
});
