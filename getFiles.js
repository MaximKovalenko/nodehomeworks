const express = require('express');
const getFiles = express.Router();
const fs = require('fs');

getFiles.get('/', (req, res) => {
    fs.readdir('./files', (err, files) => {
        if (files.length === 0) {
            res.status(400).json({message: 'Client error'});
        }
        if (!err) {
            res.status(200).json({message: 'Success', files});
        } else {
            res.status(500).json({message: 'Server error'});
            throw err;
        }
    });
});

module.exports = getFiles;
