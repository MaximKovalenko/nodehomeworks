const express = require('express');
const create = express.Router();
const fs = require('fs');
const path = require('path');

const validFile = '^.*\\.(log|txt|json|yaml|xml|js)$'

create.post('/', (req, res) => {
    const filename = req.body.filename;
    const content = req.body.content;

    if (!content) {
        return res.status(400).json({message: "Please specify 'content' parameter"});
    }
    if (filename.match(validFile)) {
        fs.writeFile(path.join(__dirname, 'files/', filename), content, (err) => {
            if (!err) {
                res.status(200).json({message: 'File created successfully'});
            } else {
                res.status(500).json({message: 'Server error'});
                throw err;
            }
        });
    } else {
        return res.status(400).json({message: "Please specify correct file type"});
    }
});

module.exports = create;
